let personsStorage = localStorage;

class Person {
  constructor(name, firstName, phone, email) {
    this.name = name;
    this.firstName = firstName;
    this.phone = phone;
    this.email = email;
  }
};

function getPersonsStorage () {
    let table = JSON.parse(personsStorage.getItem('person'))
    console.log(table.map(el => el.name))
    let contactList = document.querySelector('#contactList')
    contactList.innerHTML = table.map(el => 
      `<table id="contactTable">
      <tbody class="tbody">
       <tr>
          <th>Name: </th>
          <td class="name">${el.name}</td>
       </tr>
       <tr>
          <th>First Name: </th>
          <td class="firstName">${el.firstName}</td>
       </tr>
       <tr>
          <th>Phone: </th>
          <td class="phone">${el.phone}</td>
      </tr>
      <tr>
          <th>Email: </th>
          <td class="email">${el.email}</td>
      </tr>`
      )

  };

function addPerson () {
    let name = document.querySelector('.nameInput').value;
    let firstName = document.querySelector('.firstNameInput').value;
    let phone = document.querySelector('.phoneInput').value;
    let email = document.querySelector('.mailInput').value;

    let newPerson = new Person(name, firstName, phone, email)
    let newTablePerson = personsStorage.getItem('person') ? JSON.parse(personsStorage.getItem('person')) : []
    newTablePerson.push(newPerson)
    let jsonPerson = JSON.stringify(newTablePerson)
    personsStorage.setItem('person', jsonPerson)
    getPersonsStorage()
  };

  document.querySelector('.ajoutContactBtn').onclick = addPerson;
  getPersonsStorage();